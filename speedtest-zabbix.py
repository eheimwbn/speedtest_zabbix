#! /usr/bin/python3
# requirements 
# pip3 install speedtest-cli


"""
needs a host in zabbix named seedtest and zabbix_trap items named:
download,upload and latency.
"""
# TODO: error handling


import argparse
import subprocess

import speedtest


def zabbix_send(zabbixserver: str, host: str, key: str, value: str):
    """
    takes host, key, value and builds zabbix_send command
    :param host: zabbix internal host (exact zabbix hostname)
    :param key:  zabbix internal item key
    :param value: computed value
    :return: string from stdout
    """
    cmd_list = [
        "zabbix_sender",
        "-z",
        "zabbixserver",
        "-s",
        "host",
        "-k",
        "key",
        "-o",
        "value"]

    cmd_list[2] = zabbixserver
    cmd_list[4] = host
    cmd_list[6] = key
    cmd_list[8] = value

    return subprocess.check_output(cmd_list)


def check(zabbixserver: str, zabbix_hostname: str):
    """
        takes zabbixserver ip executes speedtest and uses zabbix_sender to send
        results
        :param zabbixserver:  zabbix fqdn or ip
        """
    servers = []
    s = speedtest.Speedtest()
    s.get_servers(servers)
    s.get_best_server()
    s.download()
    s.upload()
    results_dict = s.results.dict()
    # debug print ^^
    if args.debug is True:
        print(results_dict)
    # send download speed in Mbit/s

    print('download Mbit/s: ' + str((results_dict['download'] / 1024 / 1024)))
    zabbix_send(zabbixserver, zabbix_hostname, 'speedtest_cli_download',
                str((results_dict['download'] / 1024 / 1024)))
    print('upload Mbit/s: ' + str((results_dict['upload'] / 1024 / 1024)))
    zabbix_send(zabbixserver, zabbix_hostname, 'speedtest_cli_upload',
                str((results_dict['upload'] / 1024 / 1024)))
    print('latency ms: ' + str((results_dict['server']['latency'])))
    zabbix_send(zabbixserver, zabbix_hostname, 'speedtest_cli_latency',
                str((results_dict['server']['latency'])))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-z", help="the zabbixserver or proxy to send to ", type=str)
    parser.add_argument("--host", help="the zabbix hostname of the host item to send to", type=str)
    parser.add_argument("--debug", help="the hidden debug print ;\) ", type=bool)
    args = parser.parse_args()

    if args.debug is True:
        print(args.z)
        print(args.host)

    if args.z is None or args.host is None:
        print('\nPlease provide the zabbixhost and item hostname, use speedtest-zabbix.py -h for help\n')
        exit(1)

    check(args.z, args.host)
